package pl.map.lotnisko.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class FlightPlan {
    private Set<Flight> flights = new HashSet<>();

    public FlightPlan() {
        this.flights.add(new Flight(LocalDate.of(2019,8,8),
                new Plane(10, 100.0f, 5, 200.0f),
                FlightDirection.ARRIVAL));
        this.flights.add(new Flight(LocalDate.of(2019,8,8),
                new Plane(10, 100.0f, 5, 200.0f),
                FlightDirection.DEPARTURE));
    }
}
