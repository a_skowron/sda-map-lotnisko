package pl.map.lotnisko.model;

public enum SeatClass {
    ECONOMIC("economic"),
    BUSINESS("business");
    private final String name;

    SeatClass(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
