package pl.map.lotnisko.model;

import java.time.LocalDate;

public class Flight {
    private final LocalDate date;
    private final Plane plane;
    private final FlightDirection direction;

    public Flight(LocalDate date, Plane plane, FlightDirection direction) {
        this.date = date;
        this.plane = plane;
        this.direction = direction;
    }
}
