package pl.map.lotnisko.model;

public enum FlightDirection {
    DEPARTURE("departure"),
    ARRIVAL("arrival");
    private final String name;

    FlightDirection(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
