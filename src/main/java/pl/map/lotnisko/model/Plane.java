package pl.map.lotnisko.model;

import java.util.EnumMap;
import java.util.Map;

import static pl.map.lotnisko.model.SeatClass.BUSINESS;
import static pl.map.lotnisko.model.SeatClass.ECONOMIC;

public class Plane {
    private final Map<SeatClass, Integer> seatsNumber = new EnumMap<>(SeatClass.class);
    private final Map<SeatClass, Double> seatsPrice = new EnumMap<>(SeatClass.class);
    private final Map<SeatClass, Integer> seatsBooked = new EnumMap<>(SeatClass.class);

    public Plane(int seatsNumberEconomic, double seatsPriceEconomic, int seatsNumberBusiness, double seatsPriceBusiness) {
        this.seatsNumber.put(ECONOMIC, seatsNumberEconomic);
        this.seatsNumber.put(BUSINESS, seatsNumberBusiness);
        this.seatsPrice.put(ECONOMIC, seatsPriceEconomic);
        this.seatsPrice.put(BUSINESS, seatsPriceBusiness);
        this.seatsBooked.put(ECONOMIC, 0);
        this.seatsBooked.put(BUSINESS, 0);
    }

    public boolean book(SeatClass seatClass, Integer requiredSeats) {
        int availableSeats = seatsNumber.get(seatClass) - seatsBooked.get(seatClass);
        if (requiredSeats <= availableSeats) {
            int updatedSeatsBooked = seatsBooked.get(seatClass) + requiredSeats;
            seatsBooked.put(seatClass, updatedSeatsBooked);
            return true;
        } else {
            return false;
        }
    }
}
